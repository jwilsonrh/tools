Users making changes must include a "Signed-off-by:" tag on all commits that
acknowledges the DCO, https://developercertificate.org.
